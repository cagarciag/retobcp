FROM openjdk:8
ADD target/api-reto.jar api-reto.jar
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "api-reto.jar"]