package com.tc.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tc.reto.DTO.ExchangeDTO;
import com.tc.reto.dao.ExchangeRepository;
import com.tc.reto.model.ExchangeEntity;
import com.tc.reto.service.ExchangeService;

@RestController
@RequestMapping("api/exchange")
public class ExchangeController {
	
	@Autowired
	private ExchangeRepository exchangeRepository;
	
	@Autowired
	private ExchangeService exchangeService;
	
	@GetMapping("/findAll")
	public List<ExchangeEntity> findAll(){
		List<ExchangeEntity> list = exchangeService.buscarTodosTipoCambio();
		return list;
	}
	
	@PostMapping("/create")
	public String create(@RequestBody ExchangeEntity exchange) {
		return exchangeService.crearTipoCambio(exchange);
	}
	
	@PostMapping("/findChange")
	public ExchangeDTO findChange(@RequestBody ExchangeDTO exchangeDTO) {
		return exchangeService.convertirMoneda(exchangeDTO);
		
	}
	
}
