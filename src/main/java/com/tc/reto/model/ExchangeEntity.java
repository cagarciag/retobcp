package com.tc.reto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeEntity {
	
@Id
@GeneratedValue
private int id;
private String pais;
private String nombreMoneda;
private String moneda;
private double tcambio;
}
