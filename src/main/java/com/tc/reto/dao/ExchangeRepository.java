package com.tc.reto.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tc.reto.model.ExchangeEntity;

public interface ExchangeRepository extends JpaRepository<ExchangeEntity, Integer>{
	Optional<ExchangeEntity> findByMoneda(String moneda);
}
