package com.tc.reto.DTO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ExchangeDTO {
	private double monto;
	private String monedaOrigen;
	private String monedaDestino;
	private double montoconTipoCambio;
	private double tipoCambio;
	
	public ExchangeDTO(double monto, String monedaOrigen, String monedaDestino, double montoconTipoCambio, double tipoCambio) {
		this.monto=monto;
		this.monedaOrigen=monedaOrigen;
		this.monedaDestino=monedaDestino;
		this.montoconTipoCambio=montoconTipoCambio;
		this.tipoCambio=tipoCambio;
	}
	public ExchangeDTO() {
		
	}
}
