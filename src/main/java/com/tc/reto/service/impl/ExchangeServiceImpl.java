package com.tc.reto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tc.reto.DTO.ExchangeDTO;
import com.tc.reto.dao.ExchangeRepository;
import com.tc.reto.model.ExchangeEntity;
import com.tc.reto.service.ExchangeService;
@Service
public class ExchangeServiceImpl implements ExchangeService {
	@Autowired
	private ExchangeRepository exchangeRepository;

	@Override
	public List<ExchangeEntity> buscarTodosTipoCambio() {
		List<ExchangeEntity> result = exchangeRepository.findAll();
		return result;
	}

	@Override
	public String crearTipoCambio(ExchangeEntity exchange) {
		Optional<ExchangeEntity> listMoneda = exchangeRepository.findByMoneda(exchange.getMoneda());
		System.out.println(listMoneda);
		if(listMoneda.equals(Optional.empty())) {
			exchangeRepository.save(exchange);
			return "Ok, Tipo Cambio ingresó correctamente";
		} else {
			return "Error, Ya existe esta moneda";
		}
		
	}

	@Override
	public ExchangeDTO convertirMoneda(ExchangeDTO exchangeDTO) {
			
			Optional<ExchangeEntity> listOrigen = exchangeRepository.findByMoneda(exchangeDTO.getMonedaOrigen());
			
			Optional<ExchangeEntity> listDestino = exchangeRepository.findByMoneda(exchangeDTO.getMonedaDestino());
			
			double cambioOrigen = listOrigen.get().getTcambio();
			
			double cambioDestino = listDestino.get().getTcambio();
			
			double tipoCambio = (cambioOrigen/cambioDestino);
			
			double montoConTipoCambio = exchangeDTO.getMonto() * tipoCambio;
			
			exchangeDTO.setMontoconTipoCambio(montoConTipoCambio);
			
			exchangeDTO.setTipoCambio(tipoCambio);

		return exchangeDTO;
	}

}
