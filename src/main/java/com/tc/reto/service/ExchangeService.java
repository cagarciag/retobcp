package com.tc.reto.service;

import java.util.List;

import com.tc.reto.DTO.ExchangeDTO;
import com.tc.reto.model.ExchangeEntity;

public interface ExchangeService {
	public String crearTipoCambio(ExchangeEntity exchange);
	List<ExchangeEntity> buscarTodosTipoCambio();
	public ExchangeDTO convertirMoneda(ExchangeDTO exchangeDTO);
}
